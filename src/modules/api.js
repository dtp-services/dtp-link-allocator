const $request = require('request');
const $Promise = require('bluebird');
const $config = require('config');
const _ = require('lodash');

class API {
  static get SERVER () {
    return $config.get('services.api.url') + '/';
  }

  static async query (method, params) {
    const query = await new $Promise((resolve, reject) => {
      $request({
        method: 'post',
        url: API.SERVER + method,
        json: params || true
      }, (err, response, body) => {
        if (err) {
          return reject(err);
        }

        if (_.get(body, 'error.code') === 2) {
          return reject('INVALID_TOKEN');
        }

        resolve(body);
      });
    });

    return query;
  }
}

module.exports = API;